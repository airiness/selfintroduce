#pragma once
#include <windows.h>
#include "Frame.h"

class Scene
{
public:
	Scene(HANDLE&,HANDLE&,CHAR_INFO*,COORD);
	~Scene();

	//Draw the screen
	void Draw(const Frame& fm);
	void Swap();

private:
	void ClearHandle();

	HANDLE * current;
	HANDLE * next;
	COORD sceneSize;
	COORD sceneCoord;
	SMALL_RECT srctReadRect;
	CHAR_INFO * pixels;
};

