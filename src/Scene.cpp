#include "Scene.h"

Scene::Scene(HANDLE &buffer1, HANDLE &buffer2, CHAR_INFO *pixels_, COORD sceneSize_)
	:current(&buffer1),next(&buffer2),pixels(pixels_),sceneSize(sceneSize_),
	sceneCoord({ 0,0 }),srctReadRect({ 0,0,sceneSize_.X,sceneSize_.Y })
{
}

Scene::~Scene()
{
}

void Scene::Draw(const Frame& fm)
{
	for (const auto &p : fm.GetPoints())
	{
		if (p.isUse)
		{
			if (p.pointPosition.X >= 0 && p.pointPosition.X <= sceneSize.X
				&&p.pointPosition.Y >= 0 && p.pointPosition.Y < sceneSize.Y)
			{
				pixels[p.pointPosition.Y * sceneSize.X + p.pointPosition.X] = p.pointColor;
			}
		}
	}
	WriteConsoleOutput(*next, pixels, sceneSize, sceneCoord, &srctReadRect);
}

//Swap the buffer 
void Scene::Swap()
{
	HANDLE* tmp;
	tmp = next;
	next = current;
	current = tmp;
	SetConsoleActiveScreenBuffer(*current);
	ClearHandle();
}

void Scene::ClearHandle()
{
	for (size_t i = 0; i < sceneSize.X* sceneSize.Y; i++)
	{
		(pixels + i)->Attributes = 0X00;
		(pixels + i)->Char.AsciiChar = ' ';
	}
	WriteConsoleOutput(*next, pixels, sceneSize, sceneCoord, &srctReadRect);
}