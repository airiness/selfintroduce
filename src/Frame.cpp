#include "Frame.h"

static const DWORD timeOfAni = 3000;
static DWORD timeRiver = 0;

Frame::Frame(std::string & name)
	:globalPosition{ 0,0 }, frameName(name), fs(FrameState::MOVEIN),isOver(false)
{
	LoadWordFromFile();
	SetGloblePosition({ 0,-frameSize.Y });

}

void Frame::Update(DWORD deltaTime)
{
	switch (fs)
	{
	case MOVEIN:
		if (timeRiver < 5)
		{
			timeRiver += deltaTime;
		}
		else
		{
			if (globalPosition.Y < 0)
			{
				globalPosition.Y++;
				for (auto & p : points)
				{
					if (p.isUse)
					{
						p.pointPosition.Y++;
					}
				}
			}
			else
			{
				fs = FrameState::STAY;
			}

			timeRiver = 0;
		}
		break;
	case STAY:
		if (timeRiver < 1000)
		{
			timeRiver += deltaTime;
		}
		else
		{
			if (frameName == "ari")
			{

			}
			else
			{
				fs = FrameState::FADE;
			}
			if (frameName == "hentai")
			{
				isOver = true;
			}
			timeRiver = 0;
		}
		break;
	case FADE:
		if (timeRiver < 1000)
		{
			srand(GetTickCount());
			timeRiver += deltaTime;
			for (auto & p : points)
			{
				if (p.isUse)
				{

					p.pointColor.Attributes = 0xb0 + RandomNum(7);
					p.pointColor.Char.AsciiChar = 0x30 + RandomNum(74);
					p.pointPosition.X += ((RandomNum(2) == 0) ? 1 : -1);
					p.pointPosition.Y += ((RandomNum(5) == 0) ? -1 : 2);
				}
			}
		}
		else
		{
			fs = FrameState::OVER;
			timeRiver = 0;
		}
		break;
	case OVER:
		isOver = true;
		break;
	default:
		break;
	}
}

void Frame::SetGloblePosition(TwoDimen pos)
{
	globalPosition = pos;
	for (auto & p : points)
	{
		if (p.isUse)
		{
			p.pointPosition.X += pos.X;
			p.pointPosition.Y += pos.Y;
		}
	}
}

const std::vector<Point>& Frame::GetPoints() const
{
	return points;
}


Frame::~Frame()
{
}

int Frame::LoadWordFromFile()
{
	// file path
	std::string path{ "Resource\\" };
	path = path + frameName + ".bmp";
	//open the bmp file
	std::fstream fs(path, std::ios::binary | std::ios::in);
	//if fail return -1
	if (!fs)
	{
		return -1;
	}
	//file header
	BITMAPFILEHEADER bmpFileh;
	BITMAPINFOHEADER bmpInfoh;
	//read file header
	fs.read(reinterpret_cast<char*>(&bmpFileh), sizeof BITMAPFILEHEADER);
	//if not bmp file return -1
	if (bmpFileh.bfType != 'MB')
	{
		return -1;
	}
	//red bmp info header
	fs.read(reinterpret_cast<char*>(&bmpInfoh), sizeof BITMAPINFOHEADER);
	//bmp size (pixil)
	int bmpWidth = bmpInfoh.biWidth;
	int bmpHeight = bmpInfoh.biHeight;
	// pixil num of bmp 
	int pixilNum = bmpHeight * bmpWidth;
	//color information of bmp file
	RGBTRIPLE * pColors = new RGBTRIPLE[pixilNum];
	fs.read(reinterpret_cast<char*>(pColors), sizeof(RGBTRIPLE) * pixilNum);
	//save bmp size
	frameSize.X = bmpWidth;
	frameSize.Y = bmpHeight;

	//charInfo_ = new CHAR_INFO[pixilNum];
	for (int i = 0; i < pixilNum; i++)
	{
		auto tmpPoint = new Point();
		tmpPoint->isUse = RGBToCharInfo(*(pColors + i), (tmpPoint->pointColor));
		tmpPoint->pointPosition = { i % bmpWidth,i / bmpWidth };
		points.push_back(*tmpPoint);
	}

	fs.close();
	return 0;
}


bool Frame::RGBToCharInfo(const RGBTRIPLE & rgb, CHAR_INFO & charInfo)
{
	charInfo.Attributes = 0x00;
	charInfo.Char.AsciiChar = ' ';

	if (rgb.rgbtRed == 0x00)
	{
		if (rgb.rgbtGreen == rgb.rgbtBlue && rgb.rgbtGreen == 0x00)
		{
			return false;
		}
		else
		{
			charInfo.Attributes = 0x0f;
			charInfo.Char.AsciiChar = (rgb.rgbtGreen << 8) + rgb.rgbtBlue;
		}
	}
	else
	{
		int colorSum = rgb.rgbtBlue + rgb.rgbtGreen + rgb.rgbtRed;
		short tmp = 0x00;
		if (rgb.rgbtBlue >> 7 == 1)
		{
			tmp++;
		}
		if (rgb.rgbtGreen >> 7 == 1)
		{
			tmp += 0x2;
		}
		if (rgb.rgbtRed >> 7 == 1)
		{
			tmp += 0x4;
		}
		if (colorSum > 128 * 3)
		{
			tmp += 0x8;
		}
		charInfo.Attributes = tmp << 4;;
	}

	return true;
}

int Frame::RandomNum(int x)
{
	return rand() % x;
}
