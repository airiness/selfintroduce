#pragma once
#include<windows.h>
#include<vector>
#include<string>
#include<fstream>

struct TwoDimen 
{
	int X;
	int Y;
};

struct Point
{
	TwoDimen pointPosition;
	CHAR_INFO pointColor;
	bool isUse;
};

enum FrameState
{
	MOVEIN,
	STAY,
	FADE,
	OVER
};

class Frame
{
public:
	Frame(std::string& name);
	void Update(DWORD time);
	void SetGloblePosition(TwoDimen pos);
	const std::vector<Point>& GetPoints() const;

	bool isOver;

	~Frame();
private:
	int LoadWordFromFile();
	bool RGBToCharInfo(const RGBTRIPLE&, CHAR_INFO&);
	int RandomNum(int x);

	std::vector<Point> points;
	std::vector<Point>::iterator o = points.begin();
	std::string frameName;
	TwoDimen frameSize;
	bool isShow;
	TwoDimen globalPosition;
	FrameState fs;

}; 

