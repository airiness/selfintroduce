#include "Loop.h"
#include <fstream>
using std::fstream;
using std::pair;


Loop::Loop()
{
	Init();
}


Loop::~Loop()
{
	Uninit();
}

void Loop::MainLoop()
{
	auto previous = 0;
	while (true)
	{
		auto current = GetTickCount();
		if ((current - previous) >= 1000 / 160)
		{
			
			Update(current - previous);
			mainScene->Draw(vFrame[frameNum]);
			mainScene->Swap();
			previous = current;
		}
	}

}

void Loop::Init()
{
	//--
	std_h = CreateConsoleScreenBuffer(
		GENERIC_READ | GENERIC_WRITE,
		0,
		nullptr,
		CONSOLE_TEXTMODE_BUFFER,
		nullptr
	);
	buffer_h = CreateConsoleScreenBuffer(
		GENERIC_READ | GENERIC_WRITE,
		0,
		nullptr,
		CONSOLE_TEXTMODE_BUFFER,
		nullptr
	);
	CONSOLE_CURSOR_INFO info;
	info.dwSize = 1;
	info.bVisible = FALSE;
	SetConsoleCursorInfo(std_h, &info);
	SetConsoleCursorInfo(buffer_h, &info);
	SetConsoleTitle("���ȏЉ�");
	//--
	COORD screenSize{ g_SceneWidth ,g_sceneHeight };
	mainScene = new Scene(std_h, buffer_h, pixelsOfScene, screenSize);
	//--load frames
	fstream fframes("FramesList");
	while (!fframes.eof())
	{
		string frameName;
		fframes >> frameName;
		frameResource.insert(pair<string, Frame*>(frameName, new Frame(frameName)));
		vFrame.push_back(*frameResource[frameName]);
	}
	fframes.close();
	frameNum = 0;
}

void Loop::Uninit()
{
	delete mainScene;
}

void Loop::Update(DWORD deltaTime)
{
	if (vFrame[frameNum].isOver)
	{
		if (frameNum < vFrame.size()-1)
		{
			++frameNum;
		}
	}
	vFrame[frameNum].Update(deltaTime);
}
