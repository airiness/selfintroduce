#pragma once
#include <windows.h>
#include <unordered_map>
#include <string>
#include "Frame.h"
#include "Scene.h"
using std::unordered_map;
using std::vector;
using std::string;

const int g_SceneWidth = 120;
const int g_sceneHeight = 40;
const int g_timeGap = 8000;

class Loop
{
public:
	Loop();
	~Loop();

	void MainLoop();
private:
	void Init();
	void Uninit();
	void Update(DWORD deltaTime);

	HANDLE std_h, buffer_h;
	CHAR_INFO pixelsOfScene[g_sceneHeight * g_SceneWidth];
	unordered_map<string,Frame*> frameResource;
	vector<Frame> vFrame;
	Scene *mainScene;

private:
	int frameNum;
};

